/
Drop Table ClassType;
Drop Table Season;
Drop Table Term;
Drop Table Courses;
/

Create Table ClassType(
classType_id VARCHAR2(50) PRIMARY KEY,
classType VARCHAR2(100) NOT NULL);

Insert Into ClassType (classType_id,classType)
VALUES ('1','Concentration');
Insert Into ClassType (classType_id,classType )
VALUES ('2','GenEd');

CREATE TABLE Season(
Season_ID VARCHAR2(50) PRIMARY KEY,
season VARCHAR2(100) NOT NULL);

Insert Into Season (Season_ID,season)
VALUES ('1','Fall');
Insert Into Season (Season_ID,season)
VALUES ('2','Winter');
Insert Into Season (Season_ID,season)
VALUES ('3','Summer');

Create TABLE Term(
term_id VARCHAR2(50) PRIMARY KEY,
term VARCHAR2(50) NOT NULL,
Season_ID VARCHAR2(50) REFERENCES Season (Season_ID));

Insert Into Term (term_id,term,Season_ID)
VALUES ('01','1','1');
Insert Into Term (term_id,term,Season_ID)
VALUES ('02','2','2');
Insert Into Term (term_id,term,Season_ID)
VALUES ('3','3','1');


Create Table Courses(
Course_ID VARCHAR2(20) PRIMARY KEY,
name VARCHAR2(200) NOT NULL,
Hours NUMBER NOT NULL,
Description VARCHAR2(500) NULL,
CLH VARCHAR2(20) NOT NULL,
classType_id VARCHAR2(50) NOT NULL REFERENCES ClassType (classType_id),
term_id VARCHAR2(50) NULL REFERENCES Term (term_id));

INSERT Into Courses (Course_ID,name,Hours,Description,CLH,classType_id,term_id)
VALUES ('420-110-DW','Programming I',90,'The course will introduce the student to the basic building blocks (sequential,
selection and repetitive control structures) and modules (methods and classes)
used to write a program. The student will use the Java programming language to
implement the algorithms studied. The array data structure is introduced, and
student will learn how to program with objects.','3 - 3 - 3','1','01');

INSERT Into Courses (Course_ID,name,Hours,Description,CLH,classType_id,term_id)
VALUES ('420-210-DW','Programming II',90,'The course will introduce the student to basic object-oriented methodology in
order to design, implement, use and modify classes, to write programs in the
Java language that perform interactive processing, array and string processing,
and data validation. Object-oriented features such as encapsulation and
inheritance will be explored.','3 - 3 - 3','1','02');

COMMIT;




