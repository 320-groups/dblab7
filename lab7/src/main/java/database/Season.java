package database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Season {
    private String seasonID;
    private String season;

    public Season(String seasonID, String season){
        this.seasonID = seasonID; 
        this.season = season;
    }
    
    public String getSeasonID() {
        return this.seasonID;
    }
    public String getSeason() {
        return this.season;
    }

    public String toString(){
        return "season: " + this.season;
    }

    public void AddToDatabase(Connection conn,Season season) throws SQLException{
        try(PreparedStatement statement = conn.prepareStatement("INSERT INTO Season (Season_ID,season,Year) VALUES (?,?)")){
            statement.setString(1, season.getSeasonID());
            statement.setString(2,season.getSeason());
            statement.executeUpdate();

        } catch (SQLException e ){
            e.printStackTrace();
        }

            }
}
