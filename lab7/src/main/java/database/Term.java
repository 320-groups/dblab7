package database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Term {
    private String termID;
    private String term;
    private String seasonID;

    public Term(String termID, String term, String seasonID){
        this.termID = termID;
        this.term = term;
        this.seasonID = seasonID;
    }

    public String getTermID() {
        return this.termID;
    }

    public String getTerm() {
        return this.term;
    }

    public String getSeasonID() {
        return this.seasonID;
    }

    public String toString(){
        return "term: " + this.term;
    }

    public void AddToDatabase(Connection conn,Term term) throws SQLException{
        try(PreparedStatement statement = conn.prepareStatement("INSERT INTO Term (term_id,term,Season_ID) VALUES (?,?,?)")){
            statement.setString(1, term.getTermID());
            statement.setString(2,term.getTerm());
            statement.setString(3,term.getSeasonID());
            statement.executeUpdate();

        } catch (SQLException e ){
            e.printStackTrace();
        }

            }
    
}
