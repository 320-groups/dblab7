package database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Courses {

    private String Course_ID;
    private String name;
    private int hours;
    private String description;
    private String CLH;
    private String classTypeID;
    private String termID;

    public Courses(String Course_ID,String name, int hours, String description,String CLH, String classTypeID,String termID ){
        this.Course_ID = Course_ID;
        this.name = name;
        this.hours = hours;
        this.description = description;
        this.CLH = CLH;
        this.classTypeID = classTypeID;
        this.termID = termID;
    }

    public String getCourse_ID() {
        return this.Course_ID;
    }

    public String getName() {
        return this.name;
    }

    public int getHours() {
        return this.hours;
    }

    public String getDescription() {
        return this.description;
    }

    public String getCLH() {
        return this.CLH;
    }

    public String getClassTypeID() {
        return this.classTypeID;
    }

    public String getTermID() {
        return this.termID;
    }

    public String toString(){
        return "Course name: " + this.name + " Hours: " + this.hours + " Description: " + this.description + " CLH: " + this.CLH;
    }

    public void AddToDatabase(Connection conn) throws SQLException{
        try(PreparedStatement statement = conn.prepareStatement("INSERT INTO Courses (Course_ID,name,Hours,Description,CLH,classType_id,term_id) VALUES (?,?,?,?,?,?,?)")){
            statement.setString(1, this.Course_ID);
            statement.setString(2,this.name);
            statement.setInt(3,this.hours);
            statement.setString(4,this.description);
            statement.setString(5,this.CLH);
            statement.setString(6,this.classTypeID);
            statement.setString(7,this.termID);
            statement.executeUpdate();

            System.out.println("Succesfully added new course!");

        } catch (SQLException e ){
            e.printStackTrace();
        }

    }

}
