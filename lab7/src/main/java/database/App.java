package database;
import java.sql.SQLException;
import java.util.Scanner;

public class App {
    public static void main( String[] args )throws SQLException{
        Scanner input = new Scanner(System.in);
        System.out.println("Username: ");
        String user = input.nextLine();

        System.out.println("Password : ");
        String pass = new String(System.console().readPassword("Password: "));
        input.close();

        CourseListServices conn = new CourseListServices(user,pass);

        String description = "The course will enhance the students’ knowledge of object-oriented\r\n" + //
                "programming and Java to produce stand-alone applications employing reusable\r\n" + //
                "objects, data structures and the Java collections framework. The concepts of\r\n" + //
                "inheritance, polymorphism, data abstraction and programming to interfaces are\r\n" + //
                "used to design software. Students are introduced to software version control\r\n" + //
                "and effective team collaboration.";
        
        conn.addCourse("420-310-DW","Programming III", 90, description, "3 - 3 - 3", "1", "3", conn.getConnection());
        
    }

}
