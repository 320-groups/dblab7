package database;
import java.sql.*;
public class CourseListServices {

    private Connection conn;

    public CourseListServices(String user, String pass){
        try{
            String url = "jdbc:oracle:thin:@198.168.52.211: 1521/pdbora19c.dawsoncollege.qc.ca";
            conn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void close(){
        try{
            if(this.conn != null){
                conn.close();
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public Connection getConnection(){
        return this.conn;
    }

    public void addCourse(String Course_ID, String name, int hours, String description,String CLH, String classTypeID, String termID, Connection conn){
        try{
            Courses course = new Courses(Course_ID, name, hours, description, CLH, classTypeID, termID);
            course.AddToDatabase(conn);

        } catch (SQLException e ){
            e.printStackTrace();
        } finally {
            close();
        }
    }
    
}
