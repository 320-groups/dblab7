package database;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class ClassType {
    private String classTypeID;
    private String classType;

    public ClassType(String classTypeID,String classType){
        this.classTypeID = classTypeID;
        this.classType = classType;
    }

    public String getClassTypeID() {
        return this.classTypeID;
    }

    public String getClassType() {
        return this.classType;
    }

    public String toString(){
        return "classType: " + this.classType;
    }

    public void AddToDatabase(Connection conn,ClassType classType) throws SQLException{
        try(PreparedStatement statement = conn.prepareStatement("INSERT INTO ClassType (classType_id,classType) VALUES (?,?)")){
            statement.setString(1, classType.getClassTypeID());
            statement.setString(2,classType.getClassType());
            statement.executeUpdate();

        } catch (SQLException e ){
            e.printStackTrace();
        }

            }
    
}
